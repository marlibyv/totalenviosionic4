import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  products: any[] = [];
  textoBuscar = '';

  constructor(private dataservice: DataService, private router: Router, private navCtrl: NavController, public modalCtrl: ModalController) { }

  ngOnInit() {
    this.getListProducts();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  getDataProduct(product) {
    this.modalCtrl.dismiss({
      product: product
    })
  }

  getListProducts() {
    this.dataservice.getBox()
    .subscribe( products => {
      this.products = products;
    })
  }

  buscar(event) {
    this.textoBuscar = event.detail.value;
  }

}
