import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate{

  token : any;
  
  constructor(private router: Router, private auth: AuthenticationService, private alertCtrl: AlertController, private storage: Storage) { }
 
  async canActivate(): Promise<boolean> {
    this.token = await this.storage.get('token').then((val => {
      console.log(val, 'val');
      return val;
    }));
    console.log(this.token, 'val 2');
    if (this.token) {
/*       this.alertCtrl.create({
        header: 'Unauthorized',
        message: 'You are not allowed to access that page.',
        buttons: ['OK']
      }).then(alert => alert.present()); */
    
      this.router.navigateByUrl('/members/tab1');
      return false;
    } else {
      return true;
    }
  }
}
