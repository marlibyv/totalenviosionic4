import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable, from, of } from 'rxjs';
import { take, map, switchMap } from 'rxjs/operators';
import { JwtHelperService } from "@auth0/angular-jwt";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SERVER_URL } from 'src/environments/environment';
 
const TOKEN_KEY = 'token';
 
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public user: Observable<any>;
  private userData = new BehaviorSubject(null);
 
  constructor(private storage: Storage, private http: HttpClient, private plt: Platform, private router: Router) { 
    this.loadStoredToken();  
  }
 
  loadStoredToken() {
    let platformObs = from(this.plt.ready());
 
    this.user = platformObs.pipe(
      switchMap(() => {
        console.log('switch map get storage');
        return from(this.storage.get(TOKEN_KEY));
      })
    );
  }
 
  login(credentials: {email: string, password: string }) {
    return this.http.post<any>(SERVER_URL + 'login', credentials);
  }
 
  getUser() {
    return this.userData.getValue();
  }

  logout() {
    this.storage.remove(TOKEN_KEY).then(() => {
      this.router.navigateByUrl('/');
      this.userData.next(null);
    });
    this.storage.clear();
  }

  isAuthenticated() {
    console.log('userData', this.userData.value);
    return this.userData.value;
  }
 
}