import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { delay } from 'rxjs/operators';
import { SERVER_URL } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http:HttpClient) { }

  //Muestra las imagenes del slider
  getSlide() {
    return this.http.get<any[]>(SERVER_URL + 'image?type=S')
    .pipe(
      delay(2000)
    );
  }

  //Muestra las imagenes de las promociones
  getSlidePromo() {
    return this.http.get<any[]>(SERVER_URL + 'image?type=P')
    .pipe(
      delay(2000)
    );
  }

  getCountry() {
    return this.http.get<any[]>(SERVER_URL + 'pais/all')
  }

  getOrigin() {
    return this.http.get<any[]>(SERVER_URL + 'origen/4')
  } 
  
  getCity(value) {
    return this.http.get<any[]>(SERVER_URL + 'ciudad?estado=' + value)
  }

  getState(id) {
    return this.http.get<any[]>(SERVER_URL + 'ciudad/pais?pais=' + id)
  }

  getBox() {
    return this.http.get<any[]>(SERVER_URL + 'box/all')
    .pipe(
      delay(2000)
    );
  }

  getPayment() {
    return this.http.get<any[]>(SERVER_URL + 'payment/all')
  }

  getSeguro() {
    return this.http.get<any[]>(SERVER_URL + 'seguro')
  }

  //Enviar correo
  sendEmail(data) {
    return this.http.post<any[]>(SERVER_URL + 'email/contact', data);
  }

  //Registro
  registerData(data) {
    return this.http.post<any[]>(SERVER_URL + 'register', data);
  }

  //Minimos
  getMinValues() {
    return this.http.get<any[]>(SERVER_URL + 'pricemin/all'); 
  }
  
}
