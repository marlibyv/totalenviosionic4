import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailCalcPage } from './detail-calc.page';

describe('DetailCalcPage', () => {
  let component: DetailCalcPage;
  let fixture: ComponentFixture<DetailCalcPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCalcPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailCalcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
