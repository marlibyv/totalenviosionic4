import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailCalcPage } from './detail-calc.page';

const routes: Routes = [
  {
    path: '',
    component: DetailCalcPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailCalcPageRoutingModule {}
