import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { AuthenticationService } from '../services/authentication.service';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-detail-calc',
  templateUrl: './detail-calc.page.html',
  styleUrls: ['./detail-calc.page.scss'],
})
export class DetailCalcPage implements OnInit {

  detail: any;
  text: any;
  date : any;

  constructor(public activatedRoute: ActivatedRoute, private socialSharing: SocialSharing, private auth: AuthenticationService, private alertCtrl: AlertController) { }

  ngOnInit() {
    let dataRecv = this.activatedRoute.snapshot.paramMap.get('dataObj');
    this.detail = JSON.parse(dataRecv);
    this.date = moment().format('DD/MM/YYYY, h:mm a');
  }

  share() {
    this.text = `Cotización - Total Envíos LLC\n`;
    this.text += `${this.date}\n`;
    this.text += `-\n`;
    this.text += `Monto flete \n$${this.detail.flete}\n`;
    
    if(this.detail.totalMinAereo && this.detail.tipoenvio === 'Aéreo') {
      this.text += `Total a pagar (por mínimo) \n$${this.detail.fletemin}\n`;
    }

    if(this.detail.totalMinMaritimo && this.detail.tipoenvio === 'Marítimo') {
      this.text += `Total a pagar (por mínimo) \n$${this.detail.fletemin}\n`;
    }

    this.text += `Seguro (${this.detail.seguro}%)\n$${this.detail.montoseguro}\n`;

    if(this.detail.pais === 'Colombia') {
      this.text += `Impuesto (${this.detail.bogotaImpuesto}%)\nValor impuesto: $${this.detail.montoimpuesto}\n`;
    }

    this.text += `Otros cargos\n$${this.detail.otroscargos}\nTotal monto\n$${this.detail.totalMonto}\n`;

    if(this.detail.tipo === 0) {
      this.text += `Método de pago - ${this.detail.namePayment} (${this.detail.percent}%)\n`;
      this.text += `Comisión ${this.detail.namePayment}: $${this.detail.montometodo}\n`;
    } else {
      this.text += `Método de pago - ${this.detail.namePayment} ($${this.detail.percent})\n`;
      this.text += `Comisión ${this.detail.namePayment}: $${this.detail.montometodo}\n`;
    }

    this.text += `Total a pagar \n$${this.detail.totalapagar}\n`;
    
    if(this.detail.sistema === 1) {
      this.text += `Caja de ${this.detail.ancho} in x ${this.detail.alto} in x ${this.detail.largo} in `;
    } else {
      this.text += `Caja de ${this.detail.ancho} cm x ${this.detail.alto} cm x ${this.detail.largo} cm `;
    }

    if(this.detail.tipoenvio === 'Aéreo') {
      if(this.detail.sistema === 1) {
        this.text += `con un peso de ${this.detail.peso} lb\n`;
        this.text += `y un peso volumétrico de ${this.detail.peso_vol_aereo} lb\n\n`;
      }else {
        this.text += `con un peso de ${this.detail.peso_lb} kg\n`;
        this.text += `y un peso volumétrico de ${this.detail.peso_dec} kg\n\n`;
      }    
    } else if(this.detail.tipoenvio === 'Marítimo') {
      if(this.detail.sistema === 1) {
        this.text += `de ${this.detail.piescubicos} pie cúbico\n\n`;
      }else {
        this.text += `de ${this.detail.piescubicos} pie cúbico\n\n`;
      }  
    }

    this.text += `Tiempo estimado de entrega de: ${this.detail.days}\n`;
    this.text += `Envío: ${this.detail.tipoenvio}\nPaís: ${this.detail.pais}\nEstado: ${this.detail.estado}\nCiudad: ${this.detail.ciudad}\n`;

    if(this.detail.empresa.length > 0) {
      this.text += `Empresa: ${this.detail.empresa}\n`;
    }

    this.text += `Nombre y Apellido: ${this.detail.nombreUser}\nEmail: ${this.detail.correo}\n`;

    console.log(this.text);
    var options = {
      message: this.text
    }
    this.socialSharing.shareWithOptions(options);
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

}
