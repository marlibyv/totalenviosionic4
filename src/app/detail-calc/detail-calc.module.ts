import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailCalcPageRoutingModule } from './detail-calc-routing.module';

import { DetailCalcPage } from './detail-calc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailCalcPageRoutingModule
  ],
  declarations: [DetailCalcPage]
})
export class DetailCalcPageModule {}
