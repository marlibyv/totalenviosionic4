import { Component } from '@angular/core';
import { ImagenmodalPageModule } from '../imagenmodal/imagenmodal.module';
import { ModalController, AlertController } from '@ionic/angular';
import { ImagenmodalPage } from '../imagenmodal/imagenmodal.page';
import { DataService } from '../services/data.service';
import { AuthenticationService } from '../services/authentication.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  slidePromo: any[] = [];
  isSkeleton: boolean = false;
  user: any;

  verImagen(imagen) {
    this.modalCtrl.create({
      component: ImagenmodalPage,
      componentProps : {
        imagen : imagen
      }
    }).then(modal => modal.present())
  }

  constructor(private modalCtrl: ModalController, private dataservice: DataService, private auth: AuthenticationService,
              public alertCtrl: AlertController, private storage: Storage) {}

  ionViewWillEnter() {
    this.storage.get('user').then((val => {
      this.user = val;
      if(!this.user) {
        this.presentAlert();
      }
      console.log(this.user, 'user')
    }));
    this.getSlideImgPromo();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Su sesión expiró',
      message: 'Inicie sesión nuevamente',
      buttons: [{
        text: 'OK',
        handler: data => {
          this.auth.logout();
        }
      }]
    });

    await alert.present();
  }

  getSlideImgPromo()
  {
    this.dataservice.getSlidePromo()
    .subscribe(result => {
      this.isSkeleton = true;
      this.slidePromo = result;
      console.log(this.slidePromo);
    })
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

}
