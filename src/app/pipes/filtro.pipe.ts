import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  removeAccents(value) {
      return value
          .replace(/á/g, 'a')            
          .replace(/é/g, 'e')
          .replace(/í/g, 'i')
          .replace(/ó/g, 'o')
          .replace(/ú/g, 'u');
  }

  transform(products: any[], texto = ''): any[] {
    
    if(texto.length === 0) {
      //console.log(products);
      return products;
    }

    texto = this.removeAccents(texto.toString().toLocaleLowerCase());

    return products.filter( item => {
      return this.removeAccents(item.nombre).toString().toLocaleLowerCase().includes(texto);
    })
  }

}
