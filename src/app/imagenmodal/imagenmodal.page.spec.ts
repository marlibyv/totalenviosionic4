import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImagenmodalPage } from './imagenmodal.page';

describe('ImagenmodalPage', () => {
  let component: ImagenmodalPage;
  let fixture: ComponentFixture<ImagenmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagenmodalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImagenmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
