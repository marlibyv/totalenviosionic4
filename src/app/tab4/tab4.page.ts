import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  sliderPrincipal: any;
  sliderOne: any;
  sliderServices: any;
  user: any;

  public slideOptions = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '1.2',
    centeredSlides: false,
    loop: false,
    spaceBetween: -20,
  };

  public slideOptions_calc = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '2',
    centeredSlides: false,
    loop: false,
    spaceBetween: -20,
  };

  constructor(private auth: AuthenticationService, private alertCtrl: AlertController, private storage: Storage) {

    this.sliderPrincipal = {
      slides: [
        {
          img: 'assets/img/tracking/slide/clientes-3.jpg'
        },
        {
          img: 'assets/img/tracking/slide/clientes-2.jpg'
        },
        {
          img: 'assets/img/tracking/slide/clientes-1.jpg'
        },
        {
          img: 'assets/img/tracking/slide/clientes-5.jpg'
        },
        {
          img: 'assets/img/tracking/slide/clientes-4.jpg'
        }
      ]        
    };

    this.sliderOne = {
      slides: [
        {
          img: 'assets/img/home/ico_cajas-azul-sola2.svg',
          titulo: 'Aéreo',
          route: '/tabs/tabs/tab2/'
        },
        {
          img: 'assets/img/home/ico_cajas-azul-sola2.svg',
          titulo: 'Marítimo',
          route: '/tabs/tabs/tab2/'
        }
      ]        
    };

    this.sliderServices = {
      slides: [
        {
          img: 'assets/img/home/services/pickup.svg',
          titulo: 'Pick Up'
        },
        {
          img: 'assets/img/home/services/logistics.svg',
          titulo: 'Logística'
        },
        {
          img: 'assets/img/home/services/reempaque.svg',
          titulo: 'Reempaque'
        },
        {
          img: 'assets/img/home/services/car.svg',
          titulo: 'Vehículo'
        } ,
        {
          img: 'assets/img/home/services/oil-tank.svg',
          titulo: 'Lubricantes'
        }
      ]        
    };

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((val => {
      this.user = val;
      if(!this.user) {
        this.presentAlert();
      }
      console.log(this.user, 'user')
    }));
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Su sesión expiró',
      message: 'Inicie sesión nuevamente',
      buttons: [{
        text: 'OK',
        handler: data => {
          this.auth.logout();
        }
      }]
    });

    await alert.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

}
