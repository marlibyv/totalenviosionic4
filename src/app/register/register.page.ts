import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  data = {
    nombre : '',
    email : '',
    password : ''
  }

  constructor(private dataservice: DataService, private alertCtrl: AlertController, private router: Router, private loadingService: LoadingService) { }

  ngOnInit() {
  }

  register() {
    this.loadingService.loadingPresent();
    this.dataservice.registerData(this.data).subscribe( async res => {
      this.loadingService.loadingDismiss();
      const alert = await this.alertCtrl.create({
        header: 'Registro completado',
        message: 'Inicie sesión para acceder a su cuenta',
        buttons: ['OK']
      });
      await alert.present();
      this.router.navigateByUrl('/login');
    },
    async (error: Response) => {
      this.loadingService.loadingDismiss();
      if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: ['OK']
        });
        await alert.present();
      }
      else if(error.status === 400) {
        const alert = await this.alertCtrl.create({
          header: 'Registro fallido',
          message: 'El correo ingresado ya existe, intente con otro',
          buttons: ['OK']
        });
        await alert.present();
      }
      else {
        const alert = await this.alertCtrl.create({
          header: 'Registro fallido',
          message: 'No hemos podido registrar su usuario, intente más tarde',
          buttons: ['OK']
        });
        await alert.present();   
      }
      console.log(error);
    })
  }

}
