import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials = {
    email: '',
    password: ''
  };
 
  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private alertCtrl: AlertController,
    private storage: Storage,
    private loadingService: LoadingService
  ) {}
 
  ngOnInit() {}
 
  login() {
    this.loadingService.loadingPresent();
    this.auth.login(this.credentials).subscribe(async res => {
      if (res) {
        let storageObs = this.storage.set('token', res.token);
        let userData = res.usuario;
        userData.password = '';
        this.storage.set('user', userData);
        this.loadingService.loadingDismiss();
        console.log(storageObs, res);
        this.router.navigateByUrl('/members').then(() => {
          window.location.reload();
        });
      }
    }, async (error: Response) => {
      this.loadingService.loadingDismiss();
      if(error.status === 0) {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'Verifique su conexión a Internet e intente de nuevo',
          buttons: ['OK']
        });
        await alert.present();
      } else if(error.status === 400) {
        const alert = await this.alertCtrl.create({
          header: 'Error',
          message: 'Usuario o contraseña inválidos',
          buttons: ['OK']
        });
        await alert.present();
      } else {
        const alert = await this.alertCtrl.create({
          header: 'Fallo de conexión',
          message: 'No hemos podido conectarnos con el servidor, intente más tarde',
          buttons: ['OK']
        });
        await alert.present();   
        console.log(error);
      }
    });
  }
 
}
