import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { AuthenticationService } from '../services/authentication.service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
//import { Plugins } from '@capacitor/core';

//const { SplashScreen } = Plugins;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  sliderOne: any;
  sliderServices: any;
  slideHome: any[] = [];
  user = null;
  isSkeleton: boolean = false;
  name: any;

  public slideOptions = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '1.2',
    centeredSlides: false,
    loop: false,
    spaceBetween: -20,
  };

  public slideSkeleton = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '1',
    centeredSlides: true,
    loop: false
  };

  public slideOptions_calc = {
    effect: 'flip',
    direction: 'horizontal',
    slidesPerView: '2',
    centeredSlides: false,
    loop: false,
    spaceBetween: -20,
  };

  constructor(private iab: InAppBrowser, private dataservice: DataService, private auth: AuthenticationService, public storage: Storage, public alertCtrl: AlertController) {

    this.sliderOne = {
      slides: [
        {
          img: 'assets/img/png/ico_cajas-azul-sola-dark.png',
          titulo: 'Aéreo',
          route: '/members/tab2/1'
        },
        {
          img: 'assets/img/png/ico_cajas-azul-sola-dark.png',
          titulo: 'Marítimo',
          route: '/members/tab2/2'
        }
      ]        
    };

    this.sliderServices = {
      slides: [
        {
          img: 'assets/img/home/services/logistics.svg',
          titulo: 'Logística',
          url: 'https://totalenvios.com/service/logistica/'
        },
        {
          img: 'assets/img/home/services/reempaque.svg',
          titulo: 'Reempaque',
          url: 'https://totalenvios.com/service/reempaque/'
        },
        {
          img: 'assets/img/home/services/car.svg',
          titulo: 'Vehículo',
          url: 'https://totalenvios.com/service/vehiculos/'
        } ,
        {
          img: 'assets/img/home/services/oil-tank.svg',
          titulo: 'Lubricantes',
          url: 'https://totalenvios.com/service/lubricantes/'
        },
        {
          img: 'assets/img/home/services/pickup.svg',
          titulo: 'Otros',
          url : 'https://totalenvios.com/service/otros-servicios/'
        },
      ]        
    };

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('user').then((val => {
      this.user = val;
      if(!this.user) {
        this.presentAlert();
      }
      console.log(this.user, 'user')
    }));
    this.getSlideImg();
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Su sesión expiró',
      message: 'Inicie sesión nuevamente',
      buttons: [{
        text: 'OK',
        handler: data => {
          this.auth.logout();
        }
      }]
    });

    await alert.present();
  }

  onWebsite() {
    const option: InAppBrowserOptions = {
      footer: 'no'
    }
      this.iab.create('https://totalenvios.com/', '_system' , option);
  }

  onServices(url) {
    const option: InAppBrowserOptions = {
      footer: 'no'
    }
      this.iab.create(url, '_system' , option);
  }

  getSlideImg()
  {
    this.dataservice.getSlide()
    .subscribe(result => {
      this.isSkeleton = true;
      this.slideHome = result;
      console.log(this.slideHome);
    })
  }
 
  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

  getName() {
    let x = this.user.nombre.split(' ');
    return x[0];
  }

}
