import { Component, ViewChild } from '@angular/core';

import { Platform, IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { timer } from 'rxjs';
//import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  @ViewChild('myRouterOutlet', { static: true })routerOutlet: IonRouterOutlet;
  showSplash = true;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private auth: AuthenticationService,
    //private oneSignal: OneSignal,
  ) {
    this.initializeApp();
    this.platform.backButton.subscribe(() => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.router.url === '/members/tab1' || this.router.url === '/') {
        navigator['app'].exitApp()
      } else {
        //this.presentAlertConfirm()
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this.changeDarkMode();

      timer(3000).subscribe(() => this.showSplash = false)

      this.auth.user.subscribe(state => {
        console.log('Auth changed: ', state);
        if (state) {
          this.router.navigate(['members']);
        } else {
          this.router.navigate(['']);
        }
      });

/*       if (this.platform.is('cordova')) {
        this.setupPush();
      } */

    });
  }

 /* changeDarkMode(){
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    if (prefersDark.matches){
    document.body.classList.toggle('dark');
      }
  }*/

/*   setupPush() {
    this.oneSignal.startInit('7faf414f-aedc-4c70-8729-a70bb3ecae82', '1093951236831');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
    this.oneSignal.endInit();
  } */
}
