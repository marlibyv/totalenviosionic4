import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { ProductsPage } from '../products/products.page';
import { ModalController, AlertController } from '@ionic/angular';
import { AuthenticationService } from '../services/authentication.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  data = {
    nombre: '',
    email: '',
  };

  selectedValSucursal: number;
  selectedValCountry: number;
  selectedValPayment: number;
  selectedValState: number;
  selectedValCity: number;
  selectedValSystem: number = 1;
  selectedValShipping: number = 1;
  tipo: number;
  percent: any;
  namePayment: string;
  nameCountry: string;
  precio: number = 0;
  bogotaImpuesto: number;
  fees: any;
  user: any;

  listPago: any[] = [];
  listSucursal: any[] = [];
  listCountry: any[] = [];
  listState: any[] = [];
  listCity: any[] = [];
  min: any[] = [];
  seguro: any;
  otrosCargos: number = 0;
  nameProduct: string = '';
  l: any;
  h: any;
  w: any;
  peso_lb: any;
  ciudad: any;
  pais: any;
  isSeguro: boolean = false;
  isCharge: boolean = false;
  nombreUser: string = '';
  correo: string = '';
  empresa: string = '';

  getProducts() {
    this.router.navigateByUrl('/products');
  }

  getResults() {
    //this.router.navigateByUrl('/detail-calc');
    this.calcularcosto();
  }

  listEnvio: any[] = [
    {
      id: 1,
      name: 'Aéreo',
    },
    {
      id: 2,
      name: 'Marítimo',
    },
  ];

  listUnidad: any[] = [
    {
      id: 1,
      name: 'Sistema inglés',
    },
    {
      id: 2,
      name: 'Sistema decimal',
    },
  ];

  constructor(
    private router: Router,
    private dataservice: DataService,
    private activatedRoute: ActivatedRoute,
    public modalCtrl: ModalController,
    private auth: AuthenticationService,
    private storage: Storage,
    public alertCtrl: AlertController
  ) {}

  ionViewWillEnter() {
    this.storage.get('user').then((val => {
      this.user = val;
      if(!this.user) {
        this.presentAlert();
      }
      console.log(this.user, 'user')
    }));
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Su sesión expiró',
      message: 'Inicie sesión nuevamente',
      buttons: [{
        text: 'OK',
        handler: data => {
          this.auth.logout();
        }
      }]
    });

    await alert.present();
  }

  getListPayment() {
    this.dataservice.getPayment().subscribe((result) => {
      console.log('payment', result);
      this.listPago = result;
      this.selectedValPayment = this.listPago[0].id_payment;
      this.tipo = this.listPago[0].tipo;
      this.percent = this.listPago[0].percent;
      this.namePayment = this.listPago[0].name;
    });
  }

  getListOrigin() {
    this.dataservice.getOrigin().subscribe((result) => {
      console.log(result);
      this.listSucursal = result.splice(0, 1); //Muestra solo Miami
      this.selectedValSucursal = this.listSucursal[0].id_origen;
    });
  }

  getSeguroPercent() {
    this.dataservice.getSeguro().subscribe((result) => {
      //console.log(result);
      this.seguro = result[0].percent;
      this.fees = result[0].bogotaImpuesto;
      console.log(this.seguro);
    });
  }

  getListCountry() {
    this.dataservice.getCountry().subscribe((result) => {
      //console.log(result);
      this.listCountry = result;
      this.selectedValCountry = this.listCountry[0].id_pais;
      this.nameCountry = this.listCountry[0].nombrePais;
      if (this.nameCountry === 'Colombia') {
        this.selectedValShipping = 1;
      }
      this.getListState(this.selectedValCountry);
      console.log(result);
    });
  }

  getListState(id) {
    this.dataservice.getState(id).subscribe((result) => {
      //console.log(result, id, 'liststate');
      this.listState = result;
      this.selectedValState = this.listState[0];
      this.getListCity(this.selectedValState);
    });
  }

  getListCity(value) {
    this.dataservice.getCity(value).subscribe((result) => {
      //console.log(result);
      this.listCity = result;
      this.selectedValCity = this.listCity[0].id_ciudades_det;
      this.ciudad = this.listCity;
    });
  }
  //Cambiar país
  changeCountry(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValCountry = event.target.value;
      let pais = this.listCountry.filter(
        (a) => a.id_pais === this.selectedValCountry
      );
      this.nameCountry = pais[0].nombrePais;
      console.log(this.pais, 'pais');
      this.getListState(this.selectedValCountry);
    }
  }
  //Cambiar estado
  changeState(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValState = event.target.value;
      this.getListCity(this.selectedValState);
    }
  }

  changeCity(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValCity = event.target.value;
      this.ciudad = this.listCity.filter(
        (a) => a.id_ciudades_det === this.selectedValCity
      );
      console.log(this.ciudad, 'ciudad');
    }
  }

  changeSystem(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValSystem = event.target.value;
      this.l = this.selectedValSystem === 2 ? this.l * 2.54 : this.l / 2.54;
      this.h = this.selectedValSystem === 2 ? this.h * 2.54 : this.h / 2.54;
      this.w = this.selectedValSystem === 2 ? this.w * 2.54 : this.w / 2.54;
      this.peso_lb = parseFloat(
        this.selectedValSystem === 2
          ? Number(this.peso_lb / 2.205).toFixed(2)
          : Number(this.peso_lb * 2.205).toFixed(2)
      );
    }
  }
  //Cambio de tipo de envío
  changeShipping(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValShipping = event.target.value;
    }
  }

  //Cambio de tipo de envío
  changePayment(event) {
    if (event.target.value && event.target.value != '') {
      this.selectedValPayment = event.target.value;
      let pay = this.listPago.filter(
        (item) => item.id_payment === event.target.value
      );
      this.tipo = pay[0].tipo;
      this.percent = pay[0].percent;
      this.namePayment = pay[0].name;
      console.log(this.tipo, this.percent);
    }
  }

  //Mostrar el valor(%) del seguro
  changeSeguro(event) {
    if (this.isSeguro) {
      event.checked = true;
      console.log(this.isSeguro);
    } else {
      event.checked = false;
      console.log(this.isSeguro);
    }
  }
  //Mostrar otros cargos
  changeCharge(event) {
    if (this.isCharge) {
      event.checked = true;
      console.log(this.isCharge);
    } else {
      event.checked = false;
      console.log(this.isCharge);
    }
  }

  ngOnInit() {
    this.storage.get('user').then((val) => {
      this.data.email = val.email;
      this.data.nombre = val.nombre;
    });

    if (this.activatedRoute.snapshot.paramMap.get('envio')) {
      this.selectedValShipping = parseInt(
        this.activatedRoute.snapshot.paramMap.get('envio')
      );
    }

    this.getListPayment();
    this.getListOrigin();
    this.getListCountry();
    this.getSeguroPercent();
    this.getMin();
  }

  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: ProductsPage,
    });
    await modal.present();

    const { data } = await modal.onDidDismiss();
    console.log(data, 'data')
    if (data) {
      let product = data.product;
      if(product != undefined) {
        this.nameProduct = product.nombre;
        this.l = (this.selectedValSystem === 2
          ? product.l * 2.54
          : product.l
        ).toFixed(2);
        this.h = (this.selectedValSystem === 2
          ? product.h * 2.54
          : product.h
        ).toFixed(2);
        this.w = (this.selectedValSystem === 2
          ? product.w * 2.54
          : product.w
        ).toFixed(2);
        this.peso_lb = (this.selectedValSystem === 2
          ? product.peso_lb / 2.205
          : product.peso_lb
        ).toFixed(2);
      }
    }
    console.log('nameproduct', this.nameProduct);
  }

  //Calculo tarifas para detail-calc
  calcularPesoVolumetrico(ancho, alto, largo, sistema): number {
    if (sistema === 1) {
      /* ingles */
      console.log(ancho, alto, largo);
      return (ancho * alto * largo) / 166;
    } else {
      /* decimal */
      console.log(ancho, alto, largo);
      return ((ancho / 2.54) * (alto / 2.54) * (largo / 2.54)) / 166;
    }
  }

  calcularPiesCubicos(ancho, alto, largo, sistema) {
    if (sistema === 1) {
      /* ingles */
      console.log(ancho, alto, largo);
      return (ancho * alto * largo) / 1728;
    } else {
      /* decimal */
      console.log(ancho, alto, largo);
      return ((ancho / 2.54) * (alto / 2.54) * (largo / 2.54)) / 1728;
    }
  }

  getTarifa(ciudad, tipoenvio, origen) {
    if (tipoenvio === 1) {
      return origen === 1 ? ciudad.tarifaAir1 : ciudad.tarifaAir2;
    } else {
      return origen === 1 ? ciudad.tarifaOcean1 : ciudad.tarifaOcean2;
    }
  }

  getImpuesto() {
    if (this.precio <= this.fees.referencia) {
      console.log(
        (this.precio * this.fees.menor) / 100,
        this.bogotaImpuesto,
        'menor'
      );
      this.bogotaImpuesto = this.fees.menor;
      return ((this.precio * this.fees.menor) / 100).toFixed(2);
    } else
      console.log(
        (this.precio * this.fees.mayor) / 100,
        this.bogotaImpuesto,
        'mayor'
      );
    this.bogotaImpuesto = this.fees.mayor;
    return ((this.precio * this.fees.mayor) / 100).toFixed(2);
  }

  getMin() {
    this.dataservice.getMinValues()
    .subscribe((result) => {
      this.min = result;
      console.log('min', this.min);
    });
  }

  //Antes de calcular con el método de pago
  setSubTotal(a, b, c) {
    if (this.nameCountry === 'Colombia') {
      return a + parseFloat(b) + c + parseFloat(this.getImpuesto());
    } else {
      return a + parseFloat(b) + c;
    }
  }

  //Monto con el método de pago incluido
  setTotalMonto(a, b, c, d) {
    if (this.nameCountry === 'Colombia') {
      console.log(
        a + parseFloat(b) + c + d + parseFloat(this.getImpuesto()),
        'totalmonto'
      );
      return a + parseFloat(b) + c + d + parseFloat(this.getImpuesto());
    } else {
      console.log(a, parseFloat(b), c, d, 'totalmonto sin imp');
      return a + parseFloat(b) + c + d;
    }
  }
  getMinTarifa(ciudad, tipoenvio) {
    if (tipoenvio === 1) {
      /* AEREO */
      let min = this.min.filter((a) => a.type === 1);
      return ciudad.nombreRegion === 'Centro' ? min[0].tarifamin1 : min[0].tarifamin2;
    } else {
      /* MARITIMO */
      let min = this.min.filter((a) => a.type === 2);
      return ciudad.nombreRegion === 'Centro' ? min[0].tarifamin1 : min[0].tarifamin2;
    }
  }
  getIsMin(tipoenvio, medida: number) {
    if (tipoenvio === 1) {
      /* AEREO */
      let min = this.min.filter((a) => a.type === 1);
        /* ingles */
        return medida <= min[0].lbmin;
    } else {
      /* MARITIMO */
      let min = this.min.filter((a) => a.type === 2);
        return medida <= min[0].lbmin;
    }
  }

  pesoInLb(sistema, peso) {
    if (sistema === 2) {
      return peso * 2.205;
    }
    return peso;
  }

  calcularcosto() {
    var total = 0;
    var totalFlete = 0;
    var isPayment = 0;
    var totalFinal = 0;
    var pesoVolumetrico = 0;
    var piescubicos = 0;
    var tarifa = this.getTarifa(
      this.ciudad[0],
      this.selectedValShipping,
      this.selectedValSucursal
    );
    console.log(tarifa, 'tarifa');
    console.log('peso_lb', this.peso_lb);
    const pesolb = this.pesoInLb(this.selectedValSystem, this.peso_lb);
    console.log(pesolb, 'pesolb');

    //Sin mínimo
    //Si el envío es aéreo
    if (this.selectedValShipping === 1) {
      pesoVolumetrico = this.calcularPesoVolumetrico(
        this.w,
        this.h,
        this.l,
        this.selectedValSystem
      );
      console.log(tarifa, 'tarifa', pesoVolumetrico, 'peso volumetrico');
      totalFlete =
        pesoVolumetrico > parseFloat(pesolb)
          ? Math.round(parseFloat(pesoVolumetrico.toFixed(2))) * tarifa
          : Math.round(parseFloat(pesolb)) * tarifa; // calculo original : Math.round(parseFloat(pesolb.toFixed(2))) * tarifa;
      console.log('total normal', totalFlete);

      /* se verifica si se aplica el mínimo*/
      let isMin = this.getIsMin(
        this.selectedValShipping,
        pesoVolumetrico > parseFloat(pesolb)
            ? pesoVolumetrico
            : parseFloat(pesolb)
      )
      console.log('es mínimo', isMin );

          isPayment = this.setSubTotal(
            totalFlete,
            this.isSeguro
              ? ((this.precio * this.seguro) / 100).toFixed(2)
              : 0,
            this.isCharge ? this.otrosCargos : 0
          );
      
    } else {
     /* se calculan los pies cubicos */
     piescubicos = this.calcularPiesCubicos(
        this.w,
        this.h,
        this.l,
        this.selectedValSystem
      ); 
      totalFlete = piescubicos * tarifa;
      isPayment = this.setSubTotal(
        total,
        this.isSeguro
          ? ((this.precio * this.seguro) / 100).toFixed(2)
          : 0,
        this.isCharge ? this.otrosCargos : 0
      );
    }

    // Con Mínimo
    if (this.selectedValShipping === 1) {
      pesoVolumetrico = this.calcularPesoVolumetrico(
        this.w,
        this.h,
        this.l,
        this.selectedValSystem
      );
      console.log(pesoVolumetrico, pesolb);
      console.log(this.ciudad, 'city');
      console.log(tarifa, 'tarifa');
      total = this.getIsMin(
        this.selectedValShipping,
        pesoVolumetrico > pesolb ? pesoVolumetrico : pesolb
      ) && this.nameCountry === 'Venezuela'
        ? this.getMinTarifa(this.ciudad[0], this.selectedValShipping)
        : pesoVolumetrico > pesolb
        ? Math.round(parseFloat(pesoVolumetrico.toFixed(2))) * tarifa
        : Math.round(parseFloat(pesolb)) * tarifa; // : Math.round(parseFloat(pesolb.toFixed(2))) * tarifa;

      isPayment = this.setSubTotal(
        total,
        this.isSeguro ? ((this.precio * this.seguro) / 100).toFixed(2) : 0,
        this.isCharge ? this.otrosCargos : 0
      );
      totalFinal = this.setTotalMonto(
        total,
        this.isSeguro ? ((this.precio * this.seguro) / 100).toFixed(2) : 0,
        this.isCharge ? this.otrosCargos : 0,
        this.tipo === 0 ? (isPayment * this.percent) / 100 : this.percent
      );

      console.log(total, 'total aereo');
      console.log(isPayment, totalFinal, 'aereo');
    } else {
      piescubicos = this.calcularPiesCubicos(
        this.w,
        this.h,
        this.l,
        this.selectedValSystem
      );
      total = this.getIsMin(
        this.selectedValShipping,
        piescubicos
      ) && this.nameCountry === 'Venezuela'
        ? this.getMinTarifa(this.ciudad[0], this.selectedValShipping)
        : piescubicos * tarifa;
      isPayment = this.setSubTotal(
        total,
        this.isSeguro ? ((this.precio * this.seguro) / 100).toFixed(2) : 0,
        this.isCharge ? this.otrosCargos : 0
      );
      totalFinal = this.setTotalMonto(
        total,
        this.isSeguro ? ((this.precio * this.seguro) / 100).toFixed(2) : 0,
        this.isCharge ? this.otrosCargos : 0,
        this.tipo === 0 ? (isPayment * this.percent) / 100 : this.percent
      );

      console.log(total, 'total maritimo');
      console.log(isPayment, totalFinal, 'maritimo');
    }

    let totaldetail = {
      flete: totalFlete.toFixed(2),
      fletemin: total.toFixed(2),
      totalMonto: Number(isPayment).toFixed(2),
      totalapagar: Number(totalFinal).toFixed(2),
      seguro: this.seguro,
      montoseguro: this.isSeguro
        ? ((this.precio * this.seguro) / 100).toFixed(2)
        : 0,
      otroscargos: this.isCharge ? this.otrosCargos.toFixed(2) : 0,
      tipo: this.tipo,
      namePayment: this.namePayment,
      percent: this.percent,
      montometodo:
        this.tipo === 0
          ? ((isPayment * this.percent) / 100).toFixed(2)
          : this.percent.toFixed(2),
      pesoVolumetrico,
      piescubicos: piescubicos.toFixed(2),
      ciudad: this.ciudad[0].nombreCiudades_det,
      estado: this.ciudad[0].nombreEstadosDet,
      pais: this.nameCountry,
      tipoenvio: this.listEnvio[this.selectedValShipping - 1].name,
      nombreUser: this.data.nombre,
      correo: this.data.email,
      empresa: this.empresa,
      ancho: parseFloat(this.w).toFixed(2),
      alto: parseFloat(this.h).toFixed(2),
      largo: parseFloat(this.l).toFixed(2),
      peso_lb: this.peso_lb,
      peso_vol_aereo: pesoVolumetrico.toFixed(2), /* (Math.round(Number(pesoVolumetrico.toFixed(2)))) */
      peso_dec: (Math.round(Number(pesoVolumetrico / 2.2046))).toFixed(2),
      peso:
        pesoVolumetrico > this.peso_lb
          ? Math.round(Number(pesoVolumetrico))
          : Number(this.peso_lb).toFixed(2),
      sistema: this.selectedValSystem,
      volumetrico: pesoVolumetrico > pesolb,
      montoimpuesto: parseFloat(this.getImpuesto()),
      bogotaImpuesto: this.bogotaImpuesto,
      days: this.setDiasaEntrega(),
      totalMinAereo: this.getIsMin(
        this.selectedValShipping,
        pesoVolumetrico > parseFloat(pesolb) ? pesoVolumetrico : parseFloat(pesolb)
      ),
      totalMinMaritimo: this.getIsMin(
        this.selectedValShipping,
        piescubicos
      )
    };

    let dataString = JSON.stringify(totaldetail);
    this.router.navigate(['detail-calc', dataString]);
    console.log(dataString, 'dataString');
  }

  setDiasaEntrega() {
    switch (this.selectedValShipping) {
      case 1: {
        switch (this.nameCountry) {
          case 'Venezuela': {
            if (this.ciudad[0].nombreRegion === 'Centro') {
              return '5 días hábiles';
            } else {
              return '7 a 10 días hábiles';
            }
          }
          case 'Colombia':
            return '5 a 7 días hábiles';
          case 'Panamá':
            return '5 a 7 días hábiles';
          default:
            break;
        }
        break;
      }
      case 2: {
        switch (this.nameCountry) {
          case 'Venezuela': {
            if (this.ciudad[0].nombreRegion === 'Centro') {
              return '3 semanas';
            } else {
              return '4 semanas';
            }
          }
          case 'Panamá':
            return '15 días hábiles';
          default:
            break;
        }
        break;
      }
      default:
        break;
    }
  }

  validForm() {
    /* var regetcorreo = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    var regetname = /^[a-zA-Z\u00E0-\u00FC ]{3,40}$/; */
    if (
      /* this.nombreUser != '' && this.correo != '' && regetcorreo.test(this.correo) && regetname.test(this.nombreUser) 
        && */ this
        .w > 0 &&
      this.h > 0 &&
      this.l > 0 &&
      (this.isSeguro ? this.precio > 0 : this.precio >= 0) &&
      (this.selectedValShipping === 1 ? this.peso_lb > 0 : true)
    ) {
      return false;
    } else {
      return true;
    }
  }

  reset() {
    this.nameProduct = '';
    this.w = '';
    this.l = '';
    this.h = '';
    this.peso_lb = '';
    console.log('resetear calculadora');
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          },
        },
      ],
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }
}
