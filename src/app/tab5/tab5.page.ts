import { Component } from '@angular/core';
import { ToastController, AlertController } from '@ionic/angular';
import { DataService } from '../services/data.service';
import { AuthenticationService } from '../services/authentication.service';
import { Storage } from '@ionic/storage';
import { LoadingService } from '../services/loading.service';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page {

  dataEmail:any[] = [];
  user: any;
  error : boolean = false;

  data = {
    empresa: '',
    nombre: '',
    mensaje: '',
    telefono: '',
    email: ''
  }

  constructor(private toastCtrl:ToastController, private dataservice: DataService, private auth: AuthenticationService, private storage: Storage, 
              private loadingService: LoadingService, private alertCtrl: AlertController) {}

  enviarEmail() {
    console.log('Form submit');
    console.log(this.data);

    this.loadingService.loadingPresent();

     this.dataservice.sendEmail(this.data)
    .subscribe(result=>{
      console.log(result)
      this.dataEmail = result;
      this.loadingService.loadingDismiss();
      this.toastCtrl.create({
        message: "¡Su mensaje ha sido enviado con éxito!",
        duration: 4000,
        color: "dark",
        position: "bottom",
        buttons: [
          {
            icon: 'close',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      }).then((toastElement) => {
        toastElement.present();
      })

      this.data.empresa = '';
      this.data.nombre = '';
      this.data.mensaje = '';
      this.data.telefono = '';

      console.log(this.data, 'data');

    }, async error => {
      this.toastCtrl.create({
        message: "No se ha podido enviar su mensaje intente más tarde",
        duration: 4000,
        color: "dark",
        position: "bottom",
        buttons: [
          {
            icon: 'close',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      }).then((toastElement) => {
        toastElement.present();
      })
      this.error = true;
    }) 
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirmación',
      message: '¿Desea cerrar su sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Confirm Okay');
            this.auth.logout();
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
    this.presentAlertConfirm();
  }

  ionViewWillEnter() {
    this.storage.get('user').then((val => {
      this.user = val;
      if(!this.user) {
        this.presentAlert();
      }
      this.data.email = val.email;
      this.data.nombre = val.nombre;
      this.data.telefono = val.telefono;
      this.data.empresa = val.empresa;
      this.data.mensaje = val.mensaje
    }))
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Su sesión expiró',
      message: 'Inicie sesión nuevamente',
      buttons: [{
        text: 'OK',
        handler: data => {
          this.auth.logout();
        }
      }]
    });

    await alert.present();
  }

  valid() {
    var reget = /^((\\+91 -?)|0)?[0-9+]{11,13}$/;
    if(!reget.test(this.data.telefono) && (this.data.telefono.length >= 11 || this.data.telefono.length <= 13)) {
      return true;
    } else {
      return false;
    }
  }

}
